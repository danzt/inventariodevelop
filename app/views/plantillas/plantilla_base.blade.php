<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="description" content="Ejemplo de HTML5">
    <meta name="keywords" content="HTML5, CSS3, JavaScript">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
  
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet">
  
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    @section('css')
    @show

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title>
    	@section('title')
    		InventarioDevelop
    	@show
    </title>
  </head>

  <body>
  	<div class="content">
  		@yield('contenido', 'Sin contenido')
  	
	    <script src="/js/jquery.min.js"></script>
	    <script src="/js/bootstrap.min.js"></script>
	    <script src="/js/funciones.js"></script>
		@section('js')
		@show

		<div id="div-inicio-sesion">
	    	<div class="modal fade" id="modal-inicio-sesion" tabindex="-1" role="dialog" aria-labelledby="inicio-sesion">
		  		<div class="modal-dialog">
				    <div class="modal-content">
				      	<div class="modal-header">
				        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        	<h4 class="modal-title">Inicio de Sesion</h4>
				      	</div>
				      	
				      	<div class="modal-body">
				      		<div id="mensajes-inicio"></div>
				        	<form class="form-horizontal centrar-modal" id="formulario-inicio">
								<div class="row">
									<label for="input-inicio-usuario" class="col-sm-2 control-label">Usuario</label>
								    <div class="col-sm-7">
								      	<input type="text" class="form-control" id="input-inicio-usuario" placeholder="Usuario" name="usuario">
								    </div>
								</div>
								<br>
								<div class="row">
								    <label for="input-inicio-password" class="col-sm-2 control-label">Password</label>
								    <div class="col-sm-7">
								      	<input type="password" class="form-control" id="input-inicio-password" placeholder="Password" name="password">
								    </div>
								</div>
							</form>
				      	</div>
				      	<div class="modal-footer">
				        	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				        	<button type="button" class="btn btn-info" onclick="javascript:validar_inicio('formulario-inicio');">Iniciar Sesion</button>
				      	</div>
				    </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
	    </div>

	    <div id="div-registro">
    	<div class="modal fade" id="modal-registro" tabindex="-1" role="dialog" aria-labelledby="inicio-sesion">
	  		<div class="modal-dialog">
			    <div class="modal-content">
				      	<div class="modal-header">
				        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        	<h4 class="modal-title">Registrarse</h4>
				      	</div>
				      	
				      	<div class="modal-body">
				      		<div id="mensajes-registro"></div>
				        	<form class="form-horizontal centrar-modal" id="formulario-registro">
								<div class="row">
									<label for="input-registro-usuario" class="col-sm-2 control-label">Usuario</label>
								    <div class="col-sm-7">
								      	<input type="text" class="form-control" id="input-registro-usuario" placeholder="Usuario" name="usuario">
								    </div>
								</div>
								<br>
								<div class="row">
									<label for="input-registro-email" class="col-sm-2 control-label">Email</label>
								    <div class="col-sm-7">
								      	<input type="text" class="form-control" id="input-registro-email" placeholder="Email" name="email">
								    </div>
								</div>
								<br>
								<div class="row">
								    <label for="input-registro-password" class="col-sm-2 control-label">Password</label>
								    <div class="col-sm-7">
								      	<input type="password" class="form-control" id="input-registro-password" placeholder="Password" name="password">
								    </div>
								</div>
								<br>
								<div class="row">
								    <label for="input-registro-repassword" class="col-sm-2 control-label">Repetir password</label>
								    <div class="col-sm-7">
								      	<input type="password" class="form-control" id="input-registro-repassword" placeholder="Password" name="confirmacion_password">
								    </div>
								</div>
							</form>
				      	</div>
				      	<div class="modal-footer">
				        	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				        	<button type="button" class="btn btn-info" onclick="javascript:validar_registro('formulario-registro');">Registrar</button>
				      	</div>
				    </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
    </div>
	</div>
  </body>

</html>

@extends('plantillas.plantilla_base')

@section('title')
	InventarioDevelop - Nuevo producto
@stop

@section('contenido')

<!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="/inventario">DevelopTecnoMint-Application</a>
            </div>

            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/home">Perfil</a>
                    </li>
                    <li class="dropdown">
                        <a class="page-scroll" href="/productos" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Productos </a>
                          <ul class="dropdown-menu">
                            <li><a href="/pagar_credito">Ven</a></li>
                            <li><a href="/pagar_transferencia">Transferencia</a></li>
                            <li><a href="/pagar_debito">Targeta de Debito</a></li>
                            <li><a href="#" onclick="javascript:abrir_modal("inicio")">Iniciar Sesion</a></li>
                            <li><a href="#" onclick="javascript:prueba('registrar')">Registrarse</a></li> 
                          </ul>
                      </li>
                    <li>
                        <!--<a class="page-scroll" href="/pagar_credito">Comprar</a>-->
                      <li class="dropdown">
                        <a class="page-scroll" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Comprar </a>
                          <ul class="dropdown-menu">
                            <li><a href="/pagar_credito">Targeta de Credito</a></li>
                            <li><a href="/pagar_transferencia">Transferencia</a></li>
                            <li><a href="/pagar_debito">Targeta de Debito</a></li>
                            <li><a href="#" onclick="javascript:abrir_modal("inicio")">Iniciar Sesion</a></li>
                            <li><a href="#" onclick="javascript:prueba('registrar')">Registrarse</a></li> 
                          </ul>
                      </li>
                    </li>
                    <li>
                        <a class="page-scroll" href="/cerrar_sesion">Salir</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    
	    <section id="contact" >
	        <div class="container">
	            <div class="row">
	                <div class="col-lg-12">
	                    <h3 class="section-heading">Registro de producto</h3>
	                    <h3 class="section-subheading text-muted"></h3>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-lg-12 col-lg-offset-2">
	                    <form name="sendMessage" method="POST" action="/productos/registro" id="contactForm" novalidate="" enctype="multipart/form-data">
	                        <div class="row">
	                            <div class="col-md-6">
	                                <div class="form-group">
	                                    <input class="form-control" placeholder="Nombre *" id="nombre" name="nombre-producto" required="" data-validation-required-message="Por favor, Introduce un Nombre valido." type="text">
	                                    <p class="help-block text-danger"></p>
	                                </div>
	                                <div class="form-group">
	                                    <input class="form-control" placeholder="Precio *" id="precio" required="" data-validation-required-message="Por favor, Introduce un precio Correcto." type="text" name="precio-producto">
	                                    <p class="help-block text-danger"></p>
	                                </div>
	                                <div class="form-group">
	                                    <input class="form-control" placeholder="Cantidad *" id="cantidad" required="" data-validation-required-message="Por Favor introduce una cantidad valida." type="text" name="cantidad-producto">
	                                    <p class="help-block text-danger"></p>
	                                </div>

	                                <div class="form-group">
	                                    <input class="form-control" type="file" name="imagen-producto">
	                                    <p class="help-block text-danger"></p>
	                                </div>
	                            </div>
	                            <div class="col-md-6">
	                                <div class="form-group">
	                                    <textarea class="form-control" placeholder="Descripcion del producto" id="descripcion-producto" required="" data-validation-required-message="Por favor, Dejanos Tu comentario." name="descripcion-producto"></textarea>
	                                    <p class="help-block text-danger"></p>
	                                </div>
	                            </div>
	                            <div class="clearfix"></div>
	                            <div class="col-lg-12 text-center">
	                                <div id="success"></div>
	                                <button type="submit" class="btn btn-xl">Registrar Producto</button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
    </section>
	
@stop

@section('js')

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>
@stop
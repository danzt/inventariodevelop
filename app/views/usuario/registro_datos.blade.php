@extends('plantillas.plantilla_base')

@section('title')
    InventarioDevelop - Datos
@stop

@section('contenido')



<!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="/inventario">DevelopTecnoMint-Application</a>
            </div>

            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/home">Perfil</a>
                    </li>
                    <li class="dropdown">
                        <a class="page-scroll" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Productos</a>
                          <ul class="dropdown-menu">
                          	<li><a href="/productos">Ver productos</a></li>
                            <li><a href="/registrar_producto">Registrar producto</a></li>
                          </ul>
                    </li>
                    <li class="dropdown">
                        <a class="page-scroll" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Persona</a>
                          <ul class="dropdown-menu">
                            <li><a href="/registrar_datos">Registrar Datos Personales</a></li>
                          </ul>
                      </li>

                    <li>
                        <a class="page-scroll" href="/cerrar_sesion">Salir</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

	

	<section id="contact" >
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="section-heading">Registro datos personales</h3>
                        <h3 class="section-subheading text-muted"></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-lg-offset-2">
                        <form name="sendMessage" method="POST" action="/procesar_registro_datos" id="contactForm">
                            @if(isset($mensajes))
                                <div class="alert alert-success col-md-8">
                                    @foreach($mensajes as $m)
                                        {{$m}}
                                    @endforeach
                                </div>
                            @endif
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Nombre *" id="nombre" name="nombre" required="" data-validation-required-message="Por favor, Introduce un Nombre valido." type="text">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Apellido *" id="precio" required="" data-validation-required-message="Por favor, Introduce un apellido valido." type="text" name="apellido">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Cedula *" id="cedula" required="" data-validation-required-message="Por Favor introduce una cedula valida." type="text" name="cedula">
                                        <p class="help-block text-danger"></p>
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control" placeholder="Fecha de nacimiento *" id="fecha_nacimiento" required="" data-validation-required-message="Por Favor introduce una fecha de nacimiento valida." type="text" name="fecha_nacimiento">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Telefono *" id="telefono" required="" data-validation-required-message="Por Favor introduce un telefono valido." type="text" name="telefono">
                                        <p class="help-block text-danger"></p>
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-12 text-center">
                                    <div id="success"></div>
                                    <button type="submit" class="btn btn-xl">Registrar Datos</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                
            </div>
    </section>  

    @stop

@section('js')

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>
@stop


@extends('plantillas.plantilla_base')

	@section('title')
	    InventarioDv-Usuario
	@stop

	@section('contenido')

	<!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="/inventario">DevelopTecnoMint-Application</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/perfil">Perfil</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/signin">Sign In</a>
                    </li>
                    <li class="dropdown">
                        <a class="page-scroll" href="/productos" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Productos </a>
                          <ul class="dropdown-menu">
                            <li><a href="/pagar_credito">Ven</a></li>
                            <li><a href="/pagar_transferencia">Transferencia</a></li>
                            <li><a href="/pagar_debito">Targeta de Debito</a></li>
                            <li><a href="#" onclick="javascript:abrir_modal("inicio")">Iniciar Sesion</a></li>
                            <li><a href="#" onclick="javascript:prueba('registrar')">Registrarse</a></li> 
                          </ul>
                      </li>
                    <li>
                        <!--<a class="page-scroll" href="/pagar_credito">Comprar</a>-->
                      <li class="dropdown">
                        <a class="page-scroll" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Comprar </a>
                          <ul class="dropdown-menu">
                            <li><a href="/pagar_credito">Targeta de Credito</a></li>
                            <li><a href="/pagar_transferencia">Transferencia</a></li>
                            <li><a href="/pagar_debito">Targeta de Debito</a></li>
                            <li><a href="#" onclick="javascript:abrir_modal("inicio")">Iniciar Sesion</a></li>
                            <li><a href="#" onclick="javascript:prueba('registrar')">Registrarse</a></li> 
                          </ul>
                      </li>
                    </li>
                    <li>
                        <a class="page-scroll" href="/cerrar_sesion">Salir</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <header>
        <div class="container">
           
        </div>
    </header>


	@stop


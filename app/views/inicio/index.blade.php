@extends('plantillas.plantilla_base')

@section('title')
	InventarioDevelop - Inicio
@stop

@section('contenido')
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="/inventario">DevelopTecnoMint-Application</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/">Inicio</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="/productos">Productos</a>
                    </li>
                    <!--<li>
                        <a class="page-scroll" href="/mas">Mas</a>
                    </li>-->
                    <li>
                        <!--<a class="page-scroll" href="/pagar_credito">Comprar</a>-->
                      <li class="dropdown">
                        <a class="page-scroll" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Comprar </a>
                          <ul class="dropdown-menu">
                            <li><a href="/pagar_credito">Targeta de Credito</a></li>
                            <li><a href="/pagar_transferencia">Transferencia</a></li>
                            <li><a href="/pagar_debito">Targeta de Debito</a></li>
                          </ul>
                      </li>
                    </li>
                    <li class="dropdown">
                        <a class="page-scroll" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Usuarios</a>
                          <ul class="dropdown-menu">
                            <li><a data-toggle="modal" data-target="#modal-inicio-sesion">Iniciar sesion</a></li>
                            <li><a data-toggle="modal" data-target="#modal-registro">Registrarse</a></li>
                            
                          </ul>
                      </li>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Bienvenido a Tu Tienda Virtual!</div>
                <div class="intro-heading">Tu Mejor Eleccion</div>
                <a href="http://hostelix.gratishost.com.ve/DVTMPublicidad/DVTMPublicidad/DevelopTecnoMint-Advertising.html" class= "page-scroll btn btn-xl"> Nuestra Publicidad </a>
            </div>
        </div>
    </header>
@stop

@section('js')

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>
@stop
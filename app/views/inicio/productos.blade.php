
@extends('plantillas.plantilla_base')

@section('title')
	InventarioDevelop - Productos
@stop

@section('contenido')
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="/inventario">DevelopTecnoMint-Application</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                    	@if(Auth::check())
                        	<a class="page-scroll" href="/">Perfil</a>
                        @else
                        	<a class="page-scroll" href="/">Inicio</a>
                        @endif
                    </li>
                    <li>
                        <a class="page-scroll" href="/productos">Productos</a>
                    </li>
                    <!--<li>
                        <a class="page-scroll" href="/mas">Mas</a>
                    </li>-->
                    <li>
                        <!--<a class="page-scroll" href="/pagar_credito">Comprar</a>-->
                      <li class="dropdown">
                        <a class="page-scroll" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Comprar </a>
                          <ul class="dropdown-menu">
                            <li><a href="/pagar_credito">Targeta de Credito</a></li>
                            <li><a href="/pagar_transferencia">Transferencia</a></li>
                            <li><a href="/pagar_debito">Targeta de Debito</a></li>
                          </ul>
                      </li>
                    </li>
                    <li class="dropdown">
                        <a class="page-scroll" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Usuarios</a>
                          <ul class="dropdown-menu">
                            <li><a href="/cerrar_sesion">Cerrar sesion</a></li>
                          </ul>
                      </li>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <section id="team" >
        <div class="container">
            <div class="row">
            	<div class="col-lg-12">
                	<h1 class="page-header">Productos
                    	<small>Disponibles</small>
                	</h1>
            	</div>
        	</div>
            {{-- */$i = 0;/* --}}
            <div class="row">
            @forelse($productos as $producto)
            	@if($i == 3)<div class="row"> @endif
                <div class="col-sm-4">
                    <div class="team-member">
                        <img src="{{$producto->imagen}}" class="img-responsive img-circle" alt="">
                        <h4 style="text-transform: capitalize; font-size:18;">{{$producto->descripcion}}</h4>
                        <p class="text-muted">{{$producto->precio}} Bs</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa"></i>{{$producto->cantidad}}</a>
                            </li>
                            @if(Auth::check())
                            	<li><a href="javascript:realizar_compra({{$producto->id}})"><i class="fa fa-opencart"></i></a>
                            @else
                            	<li><a href="/inicio"><i class="fa fa-opencart"></i></a>
                            @endif
                            </li>
                        </ul>
                    </div>
                </div>
                {{-- */$i++;/*--}}
                @if($i == 3)</div>@endif
            @empty
		        	<div class="alert alert-danger" >
		        		No hay productos registrados
		        	</div>
	        @endforelse
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <p class="large text-muted">Estamos bajo la licensia de Software Libre Codigo Abierto, puedes conseguir el codigo de DevelopTecnomint-Aplication And Advertising en nuestro repositorio en Github.</p>
                </div>
            </div>
        </div>
    </section>
@stop

@section('js')

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>
    <script src="js/functiones.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
    	$('#team').css({'background-image':'url("/img/fondo1.jpg")'})
    });
    </script>
@stop
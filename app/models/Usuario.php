<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Usuario extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;


    protected $table = 'usuarios';
    protected $hidden = array('password', 'remember_token');

    
    public function setPasswordAttribute($value)
    {
        if ( ! empty ($value))
        {
            $this->attributes['password'] = Hash::make($value);
        }
    }
    public function setUsuarioAttribute($value)
    {
        if ( ! empty ($value))
        {
            $this->attributes['usuario'] = strtoupper( $value );
        }
    }
    public function setEmailAttribute($value)
    {
        if ( ! empty ($value))
        {
            $this->attributes['email'] = strtoupper( $value );
        }
    }

    public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}
}
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function(){
	return Redirect::to("/inicio");
});
Route::get("/inicio","InicioController@index")->before('guest');


/// URL para autenticacion
Route::post('/iniciar_sesion', "AutenticacionController@inicio_sesion");
Route::post("/registrarse", "AutenticacionController@registrarse");
Route::get("/cerrar_sesion", "AutenticacionController@cerrar_sesion");

Route::get('/registrar_producto', function(){
return View::make('usuario.registro_producto');

});


Route::get('/productos', function(){

	$productos = Producto::all();

	return View::make('inicio.productos')->with(array('productos'=>$productos));
});

Route::get('/registrar_datos', function(){

	return View::make('usuario.registro_datos');
})->before('FiltroAuth');


Route::post('/procesar_registro_datos', function(){
	
	$persona = new Persona;

	$persona->nombre = Input::get('nombre');
	$persona->apellido = Input::get('apellido');
	$persona->cedula = Input::get('cedula');
	$persona->fecha_nacimiento = Input::get('fecha_nacimiento');
	$persona->telefono = Input::get('telefono');
	$persona->id_usuario = Auth::User()->id;

	if($persona->save()){
		$mensajes = array('Datos Guardados con exito');
	}
	else{
		$mensajes = array('No se pudieron guardar los datos');	
	}

	return View::make('usuario.registro_datos')->with(array('mensajes'=>$mensajes));
})->before('FiltroAuth');



Route::post("/realizar_compra", function(){
	$id_p = Input::get('id_producto');
	$id_u = Auth::User()->id;


	$compra = new Compra;

	$compra->id_usuario = $id_u;
	$compra->id_producto = $id_p;

	if($compra->save()){

		$producto = Producto::find($id_p);
		$producto->cantidad -= 1;
		$producto->save();

		return Response::json(array('resultado'=>true));
	}
	else{
		return Response::json(array('resultado'=>false));	
	}

});



Route::get("/home", function(){

	try {
		$datos = Persona::where('id_usuario', '=', Auth::User()->id)->firstOrFail();
	} catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
		$datos = null;
	}

	return View::make('usuario.home_usuario')->with(array('datos'=>$datos));
})->before('FiltroAuth');


Route::controller('/productos',"ProductosController");

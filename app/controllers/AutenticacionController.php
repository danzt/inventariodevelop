<?php 
class AutenticacionController extends BaseController {

	public function inicio_sesion(){

		$datoslogin = array(
			'usuario' => Input::get('usuario'),
			'password' => Input::get('password'),
			);

		if(Auth::attempt($datoslogin)){
			return Response::json(array(
				'exito' => true,
				'mensajes' => array('Inicio de sesion completado')
			));
		}
		else{
			return Response::json(array(
				'exito' => false,
				'mensajes' => array('Los datos son incorrectos')
			));
		}
	}


	public function registrarse(){
		//reglas
    	$reglas = array(
    		'usuario' =>'required|max:50|unique:usuarios|alpha_num' ,
    		'email' =>'required|email|unique:usuarios' ,
    		'password' =>'required|min:5|same:confirmacion_password' ,
    		'confirmacion_password' => 'required'
    	);

        $campos = array('usuario'=>Input::get('usuario'),
        				'email'=>Input::get('email'),
        				'password' => Input::get('password'),
        				'confirmacion_password' => Input::get('confirmacion_password')
        );

        $mensajes = array(
 			'same' => 'El :attribute y :other deben coincidir',
 			'min' => 'El :attribute debe tener un minimo de :min caracteres!',
 			'email'=> 'El formato del :attribute es invalido',
 			'unique' => 'Este :attribute ya existe',
 		);

    	$validacion = Validator::make($campos,$reglas,$mensajes);
    	
    	if($validacion->fails()){
    		return Response::json(array(
    			'exito' => false,
    			'mensajes' => $validacion->messages()
    		));
    	}
    	else{
    		$usuario = new Usuario();

			$usuario->email = Input::get('email');
			$usuario->usuario = Input::get('usuario');
			$usuario->password = Input::get('password');

			$usuario->save();
			
    		return Response::json(array(
    			'exito'=>true,
    			'mensajes'=> array('Su usuario ha sido registrado con exito')
    		));
    	}
	}

	public function cerrar_sesion(){
		Auth::logout();
		return Redirect::to('/');
	}

}
?>
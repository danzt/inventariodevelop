<?php
class PagoCompraController extends CaseController{
	
	public function pagos()
	{
		$cantidad=Input::get("cantidad");
		$comentario=Input::get("descripcion");

		return View::make("inventario.comprado")->with(array(
			"cantidad" => $cantidad,
			"descripcion" => $comentario,
			));
	}
}
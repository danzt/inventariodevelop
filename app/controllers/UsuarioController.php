<?php 
class UsuarioController extends BaseController{
	
	public function registrar_usuario(){
		$nombre=Input::get("nombre");
		$apellido=Input::get("apellido");
		$correo=Input::get("correo");
		$edad=Input::get("edad");
			
		return View::make("inventario.informacion_procesada") ->with(array(
			"nombre"=>$nombre,
			"apellido"=>$apellido,
			"correo"=>$correo,
			"edad"=>$edad,
			));
	}

	public function mostrar_perfil(){
		return View::make("navegacion.perfil");
	}

	public function getIndex(){
		$usuarios = Usuario::all();

		return View::make('navegacion.sign_in')->with('usuarios', $usuarios);
	}

	public function getRegistro(){
		if(Auth::check()){
			return Redirect::to('/inventario');
		}
		else{
			return View::make('navegacion.sign_in');	
		}
		
	}

	public function postRegistro(){
		$campos = Input::all(); 

		$reglas = array(
			'usuario' => 'required|min:8|max:20|unique:usuarios',
			'email' => 'required|email|unique:usuarios',
			'clave' => 'required|min:8|',
		);

		$mensajes = array(
			'required' => 'El campo :attribute es obligatorio.!',
			'email' => 'El :attribute debe ser un correo valido.!',
			'unique' => 'Este :attribute ya existe, intente con otro.!',
			'min' => 'El campo :attribute no debe tener menos de :min caracteres',
			'max' => 'El campo :attribute no debe exceder de :max caracteres',
		 );

		$validar = Validator::make($campos, $reglas, $mensajes);

		if($validar->fails()){
			return Redirect::back()->withErrors($validar);
		}
		else{

			$usuario = new Usuario;

			$usuario->usuario = Input::get('usuario');
			$usuario->email = Input::get('email');
			$usuario->password = Input::get('clave');
			

			$usuario->save();
			return View::make('navegacion.perfil');
		}
	}

	public function getLogin(){
		if(Auth::check()){
			return View::make('navegacion.perfil');
		}
		else{
			return View::make('navegacion.sign_in');	
		}
		
	}

	public function postLogin(){
		
		$logindata = array(
			'usuario' => Input::get('usuario'),	
			'password' => Input::get('password')
		);

		if(Auth::attempt($logindata, Input::get('remember')))
		{
			return View::make('usuarios.perfil');
		}
		else{
			return Redirect::to('/login')->with('mensaje_error', 'Usuario o Contraseña Invalidos');
		}
	}

	public function getLogout(){
		Auth::logout();
		return Redirect::to('/login')->with('mensaje_error', 'Has cerrado Sesion con Exito.!');		
	}

	public function getActualizar($id){
		$usuario = Usuario::find($id);

		return View::make('usuarios.actualizar')->with('usuario', $usuario);
	}

	public function postActualizar($id){
		$usuario = Usuario::find($id);

		$usuario->usuario = Input::get('usuario');
		$usuario->email = Input::get('email');
		$usuario->password = Input::get('password');

		if($usuario->save()){
			return View::make('usuarios.login');
		}
		else{
			return "error";
		}
	}

	public function getEliminar($id){
		$usuario = Usuario::find($id);

		
		$usuario->delete();
		return Redirect::to('/usuarios')->with("mensaje", "Eliminado con Exito");
	
	}
}
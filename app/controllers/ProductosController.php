<?php 
class ProductosController extends BaseController {
	public function getProductos(){
		$productos = Producto::all();

		View::make('inicio.productos')->with(array('productos'=>$productos));
	}

	public function postRegistro(){
		$reglas = array('imagen-producto' => 'required',
						'nombre-producto' => 'required',
						'precio-producto' => 'required',
						'descripcion-producto' => 'required',
						'cantidad-producto' => 'required');

		$campo = array('imagen-producto' => Input::file('imagen-producto'),
					'nombre-producto' => Input::get('nombre-producto'),
					'precio-producto' => Input::get('precio-producto'),
					'descripcion-producto' => Input::get('descripcion-producto'),
					'cantidad-producto' => Input::get('cantidad-producto')
				);
		
		$validacion = Validator::make($campo, $reglas);
		
		if($validacion->fails()){
			return Redirect::back()->withErrors($validacion)->withInput();
		}else{
			$archivo = Input::file('imagen-producto');
			$extension = explode(".", $archivo->getClientOriginalName());
			$extension = $extension[count($extension)-1];
			$prod = new  Producto ;
			$name = "/img_productos/imagen[".date("h:s", time())."].".$extension;
			$archivo->move('img_productos',$name);
			$prod->imagen = $name;
			$prod->precio = Input::get('precio-producto');
			$prod->nombre = Input::get('nombre-producto');
			$prod->descripcion = Input::get('descripcion-producto');
			$prod->cantidad = Input::get('cantidad-producto');

			$prod->save();
			
			return Redirect::to('/productos');
		}
	}

	public function getDatosPersonales(){
		
	}
};


<?php
class InventarioController extends BaseController{

	
	public function home(){
		return View::make("inventario.home");
	}

	public function mostrar_catalogo(){
		return View::make("inventario.catalogo");
	}

	public function sign_in()
	{
		return View::make("navegacion.sign_in");
	}
}

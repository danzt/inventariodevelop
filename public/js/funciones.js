function genera_mensajes(objeto){
  var mensaje = '';
  var is_type = Object.prototype.toString.call(objeto.mensajes);
  if(is_type == "[object Object]"){
    for(var key in objeto.mensajes) {
       if(objeto.mensajes.hasOwnProperty(key)) {
            var obj = objeto.mensajes[key];
            for(var prop in obj){
              if(obj.hasOwnProperty(prop)){
                mensaje += '<p align="center">'+obj[prop]+'</p>';
              }
           }
        }
    }
  }
  else if(is_type == "[object Array]"){
    objeto.mensajes.map(function(mesg){ mensaje += '<p align="center">'+mesg+'</p>';});
  }

  return mensaje;
}

function generar_alert(tipo, mensaje){
  return '<div class="alert alert-'+tipo+'">'+mensaje+'</div>';
}

function validar_inicio(id_form){
  var usuario = $('#input-inicio-usuario');
  var password = $('#input-inicio-password');
  var mensajes = '';


  if(usuario.val().trim() == "" || password.val().trim() == ""){
    mensaje = '<div class="alert alert-danger">'+
              '<p style="text-align:center">Los campos Usuario y Password son obligatorios</p>'+
              '</div>';
    $('#mensajes-inicio').html(mensaje);
    
    setTimeout(function(){
        $('#mensajes-inicio').html('');
    },3000);
    
    return false;
  }
  else{
    $.ajax({
      'url': '/iniciar_sesion',
      'method': 'POST',
      'data': $('#'+id_form).serialize()
    })
    .done(function(data){
      var res = data;//JSON.parse(data);
    
      if(res.exito){
        $('#mensajes-inicio').html(generar_alert('success',genera_mensajes(res)));

        usuario.val('');
        password.val('');

        setTimeout(function(){
          window.location.href = "/home";
        },500);
      }
      else{
        $('#mensajes-inicio').html(generar_alert('danger',genera_mensajes(res)));
      }
    })
    .fail(function(xhr,status){
      $('#mensajes-inicio').html(xhr+" "+status);
    });
  }
};



var validar_registro = function(id_form){

    var regex_email= /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var tiempo = 2000;

    var usuario = $('#input-registro-usuario');
    var email = $('#input-registro-email');
    var password = $('#input-registro-password');
    var confirmar_password = $('#input-registro-repassword');

    if(usuario.val().trim() == "" || usuario.val().length < 5 || usuario.val().length > 20){
        $('#mensajes-registro').html(generar_alert("danger"," El campo usuario no debe quedar en blanco, debe contener entre 5 y 20 caracteres"));
        return false;
    }
    else if(email.val().trim() == ""){
        $('#mensajes-registro').html(generar_alert('danger',"El email no puede quedar vacio"));
        return false;
            
        if(!(regex_email.test(email.val()))){
            $('#mensajes-registro').html(generar_alert('danger',"El email es incorrecto"));
            return false;
        }
    }
    else if(password.val().trim() == "" || password.val().length < 5){
        $('#mensajes-registro').html(generar_alert('danger',"El campo password no debe quedar en blanco y debe contener como minimo 5 caracteres"));
        return false;
    }
    else if(confirmar_password.val().trim() == "" && password.val() != confirmar_password.val()){
        $('#mensajes-registro').html(generar_alert('danger',"Las contraseñas no cinciden"));
        return false
    }
    else{
        $.ajax({
          'url': '/registrarse',
          'method': 'POST',
          'data': $('#'+id_form).serialize()
        })
        .done(function(data){
          var res = data;//JSON.parse(data);
  
          if(res.exito){
            $('#mensajes-registro').html(generar_alert('success',genera_mensajes(res))); 

            $('#'+id_form).get(0).reset();

          }
          else{
            $('#mensajes-registro').html(generar_alert('danger',genera_mensajes(res)));
          }
        })
        .fail(function(xhr,status){
          $('#mensajes-registro').html(xhr+" "+status);
        });
    }
};


var realizar_compra = function(id){
        $.ajax({
            'url': '/realizar_compra',
          'method': 'POST',
          'data': {'id_producto':id},
        })
        .done(function(data){
          var res = data;//JSON.parse(data);
  
          if(res.resultado){
            window.location = '/productos';
            alert("Compra realizada con exito");
          }
          else{
            alert("Hubo un error al hacer la compra");
          }
        })
        .fail(function(xhr,status){
          alert("Hubo un error al hacer la compra["+status+"]");
        });
};